<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'nome', 'email', 'login', 'senha', 'status', 'admin'
    ];

    protected $hidden = [
        'senha'
    ];

    public function pedidos()
    {
        return $this->hasMany(Pedido::class);
    }
}

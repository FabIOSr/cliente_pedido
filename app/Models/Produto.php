<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $fillable = [
        'nome', 'descricao', 'estoque', 'valor'
    ];

    public function pedidos()
    {
        return $this->belongsToMany(Pedido::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $fillable = [
        'usuario_id', 'data', 'total', 'status'
    ];

    public function user()
    {
        return $this->belongsTo(Usuario::class);
    }

    public function pedido_produtos(){
        return $this->hasMany(PedidoProduto::class);
    }

    public function produtos()
    {
        return $this->hasMany(PedidoProduto::class)
                    ->selectRaw('produto_id, valor, sum(valor) as total, count(1) as qtd')
                    ->groupBy('produto_id', 'valor')
                    ->orderBy('produto_id', 'desc');                    
    }

    public function adicionarProduto($produto){
        //dd($this->produtos->where('produto_id', $produto->id)->count());

            return $this->pedido_produtos()->create([
                'produto_id' => $produto->id,
                'valor' => $produto->valor
            ]);
    }

    public function removerProduto($produto){

            return $this->pedido_produtos()
                ->where('produto_id', $produto->id)
                ->first()
                ->delete();
    }

}

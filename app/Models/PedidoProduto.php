<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PedidoProduto extends Model
{
    protected $fillable = [
        'produto_id', 'pedido_id', 'valor'
    ];

    protected $table = 'pedido_produto';
    
    public function produto()
    {
        return $this->belongsTo(Produto::class);
    }

    public function pedido(){
        return $this->belongsTo(Pedido::class);
    }
}

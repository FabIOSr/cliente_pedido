<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produto;

class FrontController extends Controller
{
    public function index()
    {
        $produtos = Produto::paginate();

        return view('front.index', compact('produtos'));
    }
}

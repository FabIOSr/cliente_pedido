<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{
    Auth,
    Hash
};

use App\Models\Usuario;

class AutenticacaoController extends Controller
{
    public function home()
    {
        return view('usuario.dashboard');
    }


    public function login()
    {
        return view('autenticacao.login');
    }

    public function logar(Request $request)
    {
        $dados = $request->all();

        $login = $dados['login'];
        $senha = $dados['senha'];

        $usuario = Usuario::where('login', $login)->where('status', true)->first();

        if(Auth::check() || ($usuario && Hash::check($senha, $usuario->senha))){
            Auth::login($usuario);
            return redirect(route('dashboard'));
        }

        return redirect(route('login'));
    }

    public function logout()
    {
        Auth::logout();

        \session()->flash('mensagem', "Usuário deslogado com sucesso, até mais...");
        \session()->flash('type', "success");


        return redirect(route('home'));
    }
}

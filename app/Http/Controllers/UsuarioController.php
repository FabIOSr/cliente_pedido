<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    public function registrar()
    {
        return view('usuarios.registrar');
    }



    public function salvar(Request $request)
    {
        $dados = $this->validate($request, [
            'nome' => 'required|max:100',
            'email' => 'required|max:100|unique:usuarios,email',
            'login' => 'required|max:20|unique:usuarios,login',
            'senha' => 'required|min:6'
        ]);

        $dados['senha'] = bcrypt($dados['senha']);

        Usuario::create($dados);

        \session()->flash('mensagem', "Usuário registrado com sucesso");
        \session()->flash('type', "success");

        return redirect()->route('home');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produto;
use App\Models\Pedido;
use Illuminate\Support\Facades\Auth;

class PedidoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $pedidos = Pedido::where('status','<>', 'aguardando')
        ->where('usuario_id', Auth::id())
        ->orderBy('id', 'desc')
        ->paginate();



        return view('pedidos.index', compact('pedidos'));
    }

    public function pedido()
    {

        $pedidos = Pedido::where([
            'status'=> 'aguardando',
            'usuario_id' => Auth::id()
            ])
        ->get();
        
        //dd($pedidos->has('produtos'));

        return view('pedidos.pedido', compact('pedidos'));
    }

    public function adicionarProduto(Produto $produto)
    {

        $pedido = $this->verificaCarrinho();


        if($pedido){
            $pedido = Pedido::find($pedido->id);

            $pedido->adicionarProduto($produto);
            
        }else{
            $pedido  = Pedido::create([
                'usuario_id' => Auth::id(),
                'data' => Date('Y-m-d H:i:s'),
                'status' => 'aguardando'
            ]);

            $pedido->adicionarProduto($produto);
        }

        if($pedido){
            \session()->flash('mensagem', 'Produto adicionado ao pedido');
            \session()->flash('type', 'success');
        }

        return redirect()->back();
    }


    public function finalizarPedido(Request $request, Pedido $pedido)
    {

        $pedido->load('pedido_produtos');

        $estorno = 0;

        //dd($pedido->pedido_produtos);        

        if($pedido->id == $request->input('pedido_id')){

            foreach($pedido->pedido_produtos as $item){
                //dd($item->produto_id);
                $produto = Produto::find($item->produto_id);

                //dd($produto);

                if($produto->estoque > 0){
                    $produto->decrement('estoque', 1);
                    \DB::table('pedido_produto')->where('id', $item->id)->update(['status' => 'finalizado']);
                }else{
                    \DB::table('pedido_produto')->where('id', $item->id)->update(['status' => 'cancelado']);   
                    $estorno += $produto->valor;
                    \session()->flash('cancelado', 'Um item do pedido foi cancelado devido falta de estoque');
                    \session()->flash('danger', 'danger');            
                }
            }
            //dd($produto);

            $pedido->update([
                'total' => $request->total - $estorno,
                'status' => 'finalizado'
            ]);
            
            \session()->flash('mensagem', 'Pedido concluido com sucesso');
            \session()->flash('type', 'success');

            return redirect('pedidos');

        }
    }

    public function verificaCarrinho()
    {
        return Pedido::where([
                'status'=> 'aguardando',
                'usuario_id' => Auth::id()
                ])
                ->select('id')
            ->first();
    }

    public function removerProduto(Produto $produto)
    {


        $pedido = $this->verificaCarrinho();


        if($pedido)
        {

            $pedido = Pedido::find($pedido->id);

            $pedido->removerProduto($produto);

            $verificaItens = $this->verificaCarrinho();



            if(count($verificaItens->produtos) < 1){
                $verificaItens->delete();
                
                \session()->flash('cancelado', 'Pedido cancelado');
                \session()->flash('danger', 'danger');

            }else{

                \session()->flash('mensagem', 'Produto removido do pedido');
                \session()->flash('type', 'danger');

            }
            
        }

        return redirect()->back();
    }

    public function delatlhesPedido(Pedido $pedido)
    {
        $pedido->load('pedido_produtos');
        
        return view('pedidos.detalhes', compact('pedido'))    ;
    }
    
}

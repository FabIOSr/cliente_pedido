<?php

namespace App\Http\Controllers;

use App\Models\Produto;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    public function index()
    {
        $produtos = Produto::paginate();

        return view('produtos.index', compact('produtos'));
    }

    public function create()
    {
        return view('produtos.create');
    }

    public function store(Request $request)
    {
        $dados = $this->validate($request, [
            'nome' => 'required|max:100',
            'descricao' => 'max:255',
            'estoque' => 'required',
            'valor' => 'required'
        ]);

        Produto::create($dados);

        \session()->flash('mensagem','Produto adicionado com sucesso');
        \session()->flash('type','success');

        return redirect()->route('produtos.index');

    }

    public function edit(Produto $produto)
    {
        return view('produtos.edit', compact('produto'));
    }

    public function update(Request $request, Produto $produto)
    {
        $dados = $this->validate($request, [
            'nome' => 'required|max:100',
            'descricao' => 'max:500',
            'estoque' => 'required',
            'valor' => 'required'
        ]);

        $produto->update($dados);

        \session()->flash('mensagem','Produto atualizado com sucesso');
        \session()->flash('type','success');

        return redirect()->route('produtos.index');
    }

    public function show(Produto $produto){
        return view('produtos.show', compact('produto'));
    }

    public function destroy(Produto $produto)
    {
        $produto->load('pedidos');

        if(count($produto->pedidos) > 0)
        {
            \session()->flash('mensagem','Produto não pode ser excluído, devido compras');
            \session()->flash('type','danger');
            
        }else{

            $produto->delete();

            \session()->flash('mensagem','Produto removido com sucesso');
            \session()->flash('type','success');
        }
        return redirect()->route('produtos.index');
    }
}

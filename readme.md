## Projeto Cliente Pedido

Pequeno sistema que permita realizar o cadastro de clientes (devem ser únicos) e produtos, possibilitando então a criação de pedidos (carrinho compra)

- Tela cadastro (registro do Cliente (único e-mail e login)).
- Tela CRUD de Produtos (cadastro, edição, detalhes e exclusão (somente se produto não existir em algum pedido)).
- Carrinho com adição e remoção de itens em um pedido.



## Modo de usar

- Clone o repositório com git clone
- Copie .env.example para .env e faça a configuraçao do seu banco de dados.
- Execute o comando compser install
- Execute o comando php artisan key:generate
- Execute o comando php artisan migrate --seed 
- Abra seu navegador na aplicação configurada com login e senha (admin:admin) ou (user:user)


## Contato

Para mais detalhes utilize o e-mail FabIOSr [fabio-s-ramos@hotmail.com](mailto:fabio-s-ramos@hotmail.com).

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).

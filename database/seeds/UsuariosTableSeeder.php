<?php

use Illuminate\Database\Seeder;

class UsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuarios = [
            [
                'nome' => 'Admin',
                'email' => 'admin@mail.com',
                'login' =>  'admin',
                'admin' => true,
                'senha' => bcrypt('admin')
            ],
            [
                'nome' => 'User',
                'email' => 'user@mail.com',
                'login' =>  'user',
                'admin' => false,
                'senha' => bcrypt('user')
            ]
        ];

        \App\Models\Usuario::insert($usuarios);
    }
}

<?php

use Illuminate\Database\Seeder;

class ProdutosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $produtos = [
            [
                'nome' => 'Air Cooler',
                'descricao' => 'Os “resfriadores” são peças essenciais para um melhor desempenho do PC , redução de superaquecimentos e aumento da vida útil das máquinas. Há muitos modelos no mercado, e o Air Cooler é daqueles imprescindíveis em sua loja.',
                'valor' => 352,
                'estoque' => 6
            ],

            [
                'nome' => 'Gabinetes',
                'descricao' => 'Os gabinetes passaram de meros armazenadores dos componentes internos da máquina para se tornarem itens fundamentais do “kit gamer”. Os modelos atuais apresentam uma série de inovações, tanto no design quanto na estrutura.',
                'valor' => 645,
                'estoque' => 5
            ],

            [
                'nome' => 'Cadeira Gamer',
                'descricao' => 'Optar por cadeiras para gamers será uma ótima escolha! Elas têm alta procura e seu formato e estrutura são diferenciados, mais robustos e confortáveis. Um dos produtos para loja de informática primordiais.A escolha pela cadeira gamer visa trazer conforto ao jogador nas horas e horas de jogatina. Seu material e cores são mais destacados e fiéis ao universo dos jogos. A presença de almofadas em pontos específicos auxilia na postura do usuário e evita futuras complicações lombares.',
                'valor' => 1645,
                'estoque' => 5
            ],

            [
                'nome' => 'Placas de vídeo',
                'descricao' => 'É bem provável que elas já estejam no estoque de sua loja, contudo, nossa recomendação é para que você não deixe de acompanhar as tendências e novos modelos de placas de vídeo. Lembre-se que, crescentemente, novos jogos e softwares são lançados, e cada um deles traz novas exigências ao processador, placa-mãe e, sobretudo, às placas de vídeo.',
                'valor' => 245,
                'estoque' => 15
            ],

            [
                'nome' => 'Cases para HD',
                'descricao' => 'O case para HD é útil e muito aproveitado para os modelos externos. Além de protegerem contra quedas e possíveis eventualidades, eles são capazes de manter o HD em uma temperatura agradável',
                'valor' => 352,
                'estoque' => 6
            ],

            [
                'nome' => 'Pendrives',
                'descricao' => 'Com o armazenamento e envio de arquivos pela nuvem, os pendrives perderam um pouco a utilidade. No entanto, eles ainda são um dos produtos para loja de informática extremamente necessários para muitas ações e processos. Nem pense em deixá-los de fora de sua lista de produtos para loja de informática.',
                'valor' => 45,
                'estoque' => 20
            ]            
        ];

        \App\Models\Produto::insert($produtos);
    }
}

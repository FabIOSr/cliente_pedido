@extends('shared.base')
 
@section('content')
<div class="card">
 
    <div class="card-header"><h3>Cadastre-se</h3></div>
    <div class="card-body">
        <form class="form-horizontal" method="post" action="{{ route('salvar') }}" >
            {{ csrf_field() }}
            <div class="form-group">
                <label for="nome" class="col-sm-2 control-label">Nome completo</label>
                <div class="col-sm-10">                
                    <input type="text" value="{{ old('nome') }}" class="form-control {{$errors->has('nome') ? 'is-invalid':''}}" value="{{ old('nome') }}" name="nome" placeholder="Digite seu nome" required>
                    @if($errors->has('nome'))
                        <em class="invalid-feedback">
                            {{ $errors->first('nome') }}
                        </em>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" value="{{ old('email') }}" class="form-control {{$errors->has('email') ? 'is-invalid':''}}" name="email" value="{{ old('email') }}" placeholder="Digite seu email" required>
                    @if($errors->has('email'))
                        <em class="invalid-feedback">
                            {{ $errors->first('email') }}
                        </em>
                    @endif
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label for="login" class="col-sm-2 control-label">Login</label>
                <div class="col-sm-10">
                    <input type="text" value="{{ old('login') }}" class="form-control {{$errors->has('login') ? 'is-invalid':''}}" name="login" value="{{ old('login') }}" placeholder="Digite seu login" required>
                    @if($errors->has('login'))
                        <em class="invalid-feedback">
                            {{ $errors->first('login') }}
                        </em>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="senha" class="col-sm-2 control-label">Senha</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control {{$errors->has('senha') ? 'is-invalid':''}}" name="senha" placeholder="Digite sua senha" required>
                    @if($errors->has('senha'))
                        <em class="invalid-feedback">
                            {{ $errors->first('senha') }}
                        </em>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="reset" class="btn btn-secondary">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Registrar</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
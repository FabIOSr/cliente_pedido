<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right" for="nome">Nome Produto</label>
    <div class="col-md-6">
        <input name="nome" type="text" class="form-control {{$errors->has('nome') ? 'is-invalid':''}}" value="{{ old('nome', $produto->nome ?? '') }}">
        @if($errors->has('nome'))
        <em class="invalid-feedback">
            {{ $errors->first('nome') }}
        </em>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right" for="descricao">Descricao Produto</label>
    <div class="col-md-6">
        <input name="descricao" type="text" class="form-control {{$errors->has('descricao') ? 'is-invalid':''}}" value="{{ old('descricao', $produto->descricao ?? '') }}">
        @if($errors->has('descricao'))
        <em class="invalid-feedback">
            {{ $errors->first('descricao') }}
        </em>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right" for="descricao">Valor Unitário Produto</label>
    <div class="col-md-6">
        <input name="valor" type="number" class="form-control {{$errors->has('valor') ? 'is-invalid':''}}" value="{{ old('valor', $produto->valor ?? '') }}">
        @if($errors->has('valor'))
        <em class="invalid-feedback">
            {{ $errors->first('valor') }}
        </em>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 col-form-label text-md-right" for="descricao">Estoque Produto</label>
    <div class="col-md-6">
        <input name="estoque" type="number" class="form-control {{$errors->has('estoque') ? 'is-invalid':''}}" value="{{ old('estoque', $produto->estoque ?? '') }}">
        @if($errors->has('estoque'))
        <em class="invalid-feedback">
            {{ $errors->first('estoque') }}
        </em>
        @endif
    </div>
</div>
@extends('shared.base')

@section('content')
    <div class="row justify-content-center">
        <div class="col-4">
            <img src="https://via.placeholder.com/300" alt="">
        </div>
        <div class="col-8">
            <h1>{{ $produto->nome }}</h1>
            <p class="text-justify">
                Descrição: {{ $produto->descricao }}
            </p>

            <hr>

            <p>Valor: {{ number_format($produto->valor, 2,',','.') }}</p>

            <p>
                @if($produto->estoque)
                <form action="{{ route('produtos.adicionarCarrinho', $produto) }}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-primary">Adicionar Carrinho</button>
                </form>
                @else
                <button disabled="disabled" class="btn btn-danger">Produto Indisponivel</button>
                @endif
            </p>
        </div>
    </div>
@endsection
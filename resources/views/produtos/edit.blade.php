@extends('shared.base')
 
@section('content')
<div class="container">
    <br>

    <div class="row justify-content-center">

        <div class="col">
            <form action="{{ route('produtos.atualizar', $produto) }}" class="mt-5" method="post">
                @csrf

                @include('produtos._form')

                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-3">

                        <button type="submit" class="btn btn-primary">
                            Atualizar Produto
                        </button>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
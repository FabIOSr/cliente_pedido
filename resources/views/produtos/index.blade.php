@extends('shared.base')
 
@section('content')
    <div class="table-responsive">
        <div class="mb-5">
            <span class="float-right">
                <a href="{{ route('produtos.criar') }}" class="btn btn-success">Adicionar Produto</a>
            </span>
        </div>
        @if(count($produtos))
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Produto</th>
                    <th>Estoque</th>
                    <th>Valor Unitário</th>
                    <th>Valor Total</th>
                    <th>Opções</th>
                </tr>
            </thead>
            <tbody>
                @foreach($produtos as $produto)
                <tr>
                    <td scope="row">{{ $produto->id }}</td>
                    <th>{{ $produto->nome }}</th>
                    <th>{{ $produto->estoque }}</th>
                    <th>{{ $produto->valor }}</th>
                    <th>{{ number_format($produto->estoque * $produto->valor, 2) }}</th>
                    <th>
                        <div class="list-inline">
                            <span class="list-inline-item">
                                <a href="{{ route('produtos.editar', $produto) }}" class="btn btn-sm btn-primary">Editar</a>
                            </span>
                            <span class="list-inline-item">
                                <form action="{{ route('produtos.excluir', $produto) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-danger">Remover</button>
                                </form>
                            </span>
                        </div>
                    </th>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else 
        <h3>Nenhum produto registrado.</h3>
        @endif
    </div>
@endsection
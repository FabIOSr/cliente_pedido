@extends('shared.base')
 
@section('content')
<div class="card">
 
<div class="card-header"><h3>Entrar no sistema</h3></div>
<div class="card-body">
<form class="form-horizontal" method="post" action="{{ route('logar') }}">
{{ csrf_field() }}
  <div class="form-group row">
    <label for="login" class="col-sm-2 control-label text-md-right">Login</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="login" placeholder="Digite seu login" autocomplete="off">
    </div>
  </div>
  <div class="form-group row">
    <label for="senha" class="col-sm-2 control-label text-md-right">Senha</label>
    <div class="col-sm-8">
      <input type="password" class="form-control" name="senha" placeholder="Digite seu senha">
    </div>
  </div>
  <div class="form-group row">
    <label for="" class="col-sm-2"></label>
    <div class="col-sm-8">
        <button type="reset" class="btn btn-secondary">Cancelar</button>
        <button type="submit" class="btn btn-primary">Enviar</button>
    </div>
  </div>
</form>
</div>
</div>
@endsection
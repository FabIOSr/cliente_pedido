<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
    <title>:: MyApps :: </title>
 
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <style>
        /* body { padding-top: 70px; } */
    </style>
</head>
 
<body>
 
<nav class="navbar navbar-expand-md navbar-dark bg-dark navbar-laravel">
    <div class="container">
        <a href="/" class="navbar-brand">MyApps</a> 
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        @if(auth()->check())
            <ul class="navbar-nav mr-auto">
                @if(auth()->user()->admin)
                <li class="nav-item">
                    <a class="nav-link text-success" href="{{ route('produtos.index') }}"><b>Produtos</b></a>
                </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link text-primary" href="{{ route('pedidos') }}"><b>Meus Pedidos</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link warning" href="{{ route('pedido') }}"><b>Meu Carrinho</b></a>
                </li>
            </ul>
        @endif
            <ul class="navbar-nav ml-auto">
            @if (Auth::check())
                <li class="nav-item"><a class="nav-link" href="#">Olá, {{ Auth::user()->login }}</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('logout') }}">Logout</a></li>
            @else
                @if(Route::has('registrar'))
                <li class="nav-item"><a class="nav-link" href="{{route('registrar')}}">Registrar</a></li>
                @endif
                <li class="nav-item"><a class="nav-link" href="{{route('login')}}">Login</a></li>
            @endif
            </ul>
        </div>
    </div>
</nav>
<div class="container pt-2">

    @if (session('cancelado'))
    <div class="alert alert-{{ session('danger') }}">
        {{ session('cancelado') }}
    </div>
    @endif

    @if (session('mensagem'))
    <div class="alert alert-{{ session('type') }}">
        {{ session('mensagem') }}
    </div>
    @endif



    @yield('content')
    
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</div>
 
</body>
 
</html>

@extends('shared.base')

@section('content')
    <h1>Pedidos</h1>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="table-responsive">

                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Data</th>
                                <th>Valor Total</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pedidos as $pedido)
                                <tr>
                                    <td>
                                        <a href="{{ route('pedidos.detalhes', $pedido) }}" class="btn btn-link" data-toggle="tooltip" title="Detalhes" data-placement="left">P{{ $pedido->id }}</a>
                                    </td>
                                    <td>
                                            {{ mb_convert_case(utf8_encode(\Carbon\Carbon::parse($pedido->data)->formatLocalized('%d, %B %Y')),MB_CASE_TITLE, 'UTF-8') }}
                                    </td>
                                    <td>R${{ number_format($pedido->total, 2,',','.') }}</td>
                                    <td>{{ ucfirst($pedido->status) }}</td>
                                    <td></td>
                                 </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div>                
            </div>
        </div>
    </div>
@endsection
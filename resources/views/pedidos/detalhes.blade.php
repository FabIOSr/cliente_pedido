@extends('shared.base')

@section('content')
    <h1>Detalhes pedido Nº {{ $pedido->id }}</h1>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Produto</th>
                    <th>Valor Unitário</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pedido->pedido_produtos as $item)
                    <tr>
                        <td>{{ $item->produto->nome }}</td>
                        <td>R${{ number_format($item->produto->valor, 2,',','.') }}</td>
                        <td>{{ ucfirst($item->status) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
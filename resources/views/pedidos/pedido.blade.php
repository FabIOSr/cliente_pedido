@extends('shared.base')

@section('content')
    <h1>Pedido em andamento</h1>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="table-responsive">
                    @if(count($pedidos))

                    @foreach($pedidos as $pedido)
                    <span> <strong>Pedido Nº:</strong> {{ $pedido->id }}</span>
                    <span class="float-right"> <strong>Data:</strong> {{ mb_convert_case(utf8_encode(\Carbon\Carbon::parse($pedido->created_at)->formatLocalized('%d, %B %Y')),MB_CASE_TITLE, 'UTF-8') }}</span>
                    <p> <strong>Status:</strong> <span class="text-danger"> {{ ucfirst($pedido->status) }}</span></p>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>QTD</th>
                                <th>Produto</th>
                                <th>Valor Unitario</th>
                                <th>Valor Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $total_pedido =0;
                            @endphp
                            @foreach ($pedido->produtos as $item)
                                <tr>
                                    <td>
                                        {{-- <img src="https://via.placeholder.com/10" class="card-img-top" alt="..."> --}}
                                        P{{ $pedido->id }}I{{ $item->produto->id }}
                                    </td>
                                    <td>
                                        <div class="list-inline">
                                            <span class="list-inline-item">
                                                <form action="{{ route('produtos.removerCarrinho', $item->produto->id) }}" id="carrinhoRemoverProduto{{ $item->produto->id }}" method="POST">
                                                    @csrf
                                                    <a class="btn btn-sm" href="#" onclick="document.getElementById('carrinhoRemoverProduto{{ $item->produto->id }}').submit()" title="Remover produto">
                                                        <i class="material-icons small"> remove_circle_outline </i>
                                                    </a>
                                                </form>
                                            </span>
                                            <span class="list-inline-item">{{ $item->qtd }}</span>
                                            <span class="list-inline-item"> 
                                                <form action="{{ route('produtos.adicionarCarrinho',  $item->produto->id) }}" id="carrinhoAdicionarProduto{{ $item->produto->id }}" method="POST">
                                                    @csrf
                                                    <a class="btn btn-sm" href="#" onclick="document.getElementById('carrinhoAdicionarProduto{{ $item->produto->id }}').submit()" title="Adicionar mais deste Produto">
                                                        <i class="material-icons small"> add_circle_outline </i>
                                                    </a>
                                                </form>
                                            </span>
                                        </div>
                                    </td>
                                    <td>{{ $item->produto->nome }}</td>
                                    <td>R${{ number_format($item->produto->valor, 2,',','.') }}</td>
                                    <td>R${{ number_format($item->total, 2,',','.') }}</td>
                                    @php
                                        $total_pedido += $item->total;
                                    @endphp
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="1">
                                    <a href="/" class="btn btn-primary btn-block btn-lg">Continuar Comprando</a>
                                </td>
                                <td colspan="2">
                                    <form action="{{ route('finalizarPedido', $pedido) }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="pedido_id" value="{{ $pedido->id }}">
                                        <input type="hidden" name="total" value="{{ $total_pedido }}">
                                        <button type="submit" class="btn btn-success btn-block btn-lg">Finalizar Pedido</button>                         
                                    </form>
                                    </td>
                                <td colspan="1">
                                    <span class="float-right"><strong>Total Pedido:</strong></span>
                                </td>
                                <td>R$ {{ number_format($total_pedido, 2,',','.') }}</td>
                            </tr>
                        </tbody>
                    </table>
                    @endforeach

                    @else
                    <div class="alert alert-info">
                        <h5> Olá  {{ Auth::user()->nome }}, você não possui nenhum pedido em andamento</h5>
                    </div>
                    @endisset
                </div>                
            </div>
        </div>
    </div>
@endsection
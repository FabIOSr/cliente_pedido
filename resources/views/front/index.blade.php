@extends('shared.base')
 
@section('content')

<div class="container">
    <div class="row">
        @foreach($produtos as $produto)
        <div class="col-3 mb-3">
            <div class="card">
                <img src="https://via.placeholder.com/250" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">{{ $produto->nome }}</h5>

                    <p class="card-text">{{ substr($produto->descricao, 0, 25) }}...</p>

                    <div class="list-inline">
                        <span class="list-inline-item">
                            <a href="{{ route('produtos.detalhes', $produto) }}" class="btn btn-primary btn-sm">Ver mais</a>                                
                        </span>
                        <span class="list-inline-item">
                            @if($produto->estoque)
                            <form action="{{ route('produtos.adicionarCarrinho', $produto) }}" method="post">
                                @csrf
                                <button type="submit" class="btn btn-success btn-sm">Adicionar Carrinho</button>
                            </form>
                            @else
                            <button disabled="disabled" class="btn btn-danger btn-sm">Indisponivel</button>
                            @endif
                        </span>
                    </div>
                </div>
            </div>  
        </div> 
        @endforeach  
    </div>
</div>

@endsection
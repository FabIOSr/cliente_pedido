<?php


Route::get('/registrar', 'UsuarioController@registrar')->name('registrar');
Route::post('/salvar', 'UsuarioController@salvar')->name('salvar');

Route::get('/login', 'AutenticacaoController@login')->name('login');
Route::post('/logar', 'AutenticacaoController@logar')->name('logar');
Route::get('/logout', 'AutenticacaoController@logout')->name('logout');

Route::get('/' , 'FrontController@index')->name('home');

Route::get('/produtos/detalhes/{produto}', 'ProdutoController@show')->name('produtos.detalhes');

Route::middleware(['auth'])->group(function(){

    Route::get('/dashboard', 'FrontController@index')->name('dashboard');

    
    Route::post('/produtos/adicionar/{produto}', 'PedidoController@adicionarProduto')->name('produtos.adicionarCarrinho');
    Route::post('/produtos/remover/{produto}', 'PedidoController@removerProduto')->name('produtos.removerCarrinho');

    Route::get('/pedidoCarrinho', 'PedidoController@pedido')->name('pedido');
    Route::get('/pedidos', 'PedidoController@index')->name('pedidos');
    Route::post('/pedidoFinalizar/{pedido}', 'PedidoController@finalizarPedido')->name('finalizarPedido');

    Route::get('/pedidos/detalhes/{pedido}', 'PedidoController@delatlhesPedido')->name('pedidos.detalhes');

    Route::prefix('admin')->group(function(){

        // PRODUTOS
        Route::get('produtos', 'ProdutoController@index')->name('produtos.index');
        Route::get('produtos/criar', 'ProdutoController@create')->name('produtos.criar');
        Route::post('produtos/adicionar', 'ProdutoController@store')->name('produtos.adicionar');
        Route::get('produtos/edicao/{produto}', 'ProdutoController@edit')->name('produtos.editar');
        Route::post('produtos/atualizar/{produto}', 'ProdutoController@update')->name('produtos.atualizar');
        Route::delete('produtos/excluir/{produto}', 'ProdutoController@destroy')->name('produtos.excluir');
    });
});
